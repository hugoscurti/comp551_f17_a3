from keras.optimizers import (Adam, sgd)
from keras.models import (Sequential, load_model)
from keras.callbacks import (EarlyStopping, ModelCheckpoint, TensorBoard, ReduceLROnPlateau)
from keras.layers import (Conv2D, MaxPooling2D, Dense, Dropout, Flatten)
import numpy as np
import keras


# Declare callbacks
callbacks = [
    ModelCheckpoint('model_checkpoints/vgg_model-{epoch:02d}-{val_loss:.2f}.hdf5', 
                    monitor='val_loss', verbose=1, period=1, 
                    save_best_only=False, save_weights_only=False),
    # TensorBoard(log_dir='./logs', histogram_freq=1, batch_size=batch_size, 
    #                 write_graph=True, write_grads=True, write_images=True, 
    #                 embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None),
    ReduceLROnPlateau(monitor='val_acc', factor=0.2,
                            patience=5, min_lr=0.0001)
    # EarlyStopping(monitor='val_acc', min_delta=0, patience=5, verbose=1)
]

def vgg_from_file(x_test, file='model_checkpoints/final_vgg_model.h5', train=False, x_train=None, y_train=None, batch_size=32, epochs=10):
    """Load model from file passed in parameters, predict x_test with this model"""
    model = load_model(file)

    if train:
        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_split=0.1, callbacks=callbacks)
        model.save('model_checkpoints/final_vgg_model.h5')

    predictions = model.predict(x_test,verbose=1)
    return predictions


def vgg(x_train,y_train,x_test=None,im_size=64,num_classes=40,batch_size=32,epochs=10):
    model = Sequential()

    #choose size of filter and size of pooling
    kernel_size = 3
    pool_size = 2

    #choose number of filters per layer
    nb_filter_1 = 32
    nb_filter_2 = 64
    nb_filter_3 = 128
    nb_filter_4 = 256
    nb_filter_5 = 512


    #add the first block of layers
    model.add(Conv2D(filters=nb_filter_1,kernel_size=(kernel_size,kernel_size),activation='relu',input_shape=(im_size,im_size,1),padding='same'))
    model.add(Conv2D(filters=nb_filter_1,kernel_size=(kernel_size,kernel_size),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(pool_size,pool_size)))

    #add second block
    model.add(Conv2D(filters=nb_filter_2,kernel_size=(kernel_size,kernel_size),activation='relu',padding='same'))
    model.add(Conv2D(filters=nb_filter_2,kernel_size=(kernel_size,kernel_size),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(pool_size,pool_size)))

    #add third block
    model.add(Conv2D(filters=nb_filter_3,kernel_size=(kernel_size,kernel_size),activation='relu',padding='same'))
    model.add(Conv2D(filters=nb_filter_3,kernel_size=(kernel_size,kernel_size),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(pool_size,pool_size)))

    #potential 4th layer, for now we keep it out
    model.add(Conv2D(filters=nb_filter_4, kernel_size=(kernel_size, kernel_size), activation='relu',padding='same'))
    model.add(Conv2D(filters=nb_filter_4, kernel_size=(kernel_size, kernel_size), activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(pool_size, pool_size)))

    # potential 5th layer
    #model.add(Conv2D(filters=nb_filter_5, kernel_size=(kernel_size, kernel_size), activation='relu', padding='same'))
    #model.add(Conv2D(filters=nb_filter_5, kernel_size=(kernel_size, kernel_size), activation='relu', padding='same'))
    #model.add(MaxPooling2D(pool_size=(pool_size, pool_size)))

    #last layers
    model.add(Flatten())
    model.add(Dropout(0.25))
    model.add(Dense(1024,activation='relu'))
    model.add(Dense(num_classes,activation='softmax'))

    model.summary()
    
    #final steps
    model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=["accuracy"])
    history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_split=0.1, callbacks=callbacks)
    print(history)

    # save model
    model.save('model_checkpoints/final_vgg_model.h5')

    predictions = model.predict(x_test, verbose=1)
    return predictions, history


