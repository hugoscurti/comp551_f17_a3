import scipy.io
from scipy.misc import imresize
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import load

from sklearn.model_selection import train_test_split
import preprocess
import math

import tempfile


def batch_split(nb_elem, batch_size):
    """Generator that returns batch splits while going through all indices < nb_elem"""
    for i in range(math.ceil(nb_elem/batch_size)):
        beginning = i*batch_size
        end = i*batch_size + batch_size
        if end > nb_elem:
            end = nb_elem
        yield (beginning, end, i)


# The code below is strongly based on Tensorflow's tutorial on Deep MNIST 
# https://www.tensorflow.org/get_started/mnist/pros
def deepnn(x, nb_classes):
  """deepnn builds the graph for a deep net for classifying digits.
  Args:
    x: an input tensor with the dimensions (N_examples, 784), where 784 is the
    number of pixels in a standard MNIST image.
  Returns:
    A tuple (y, keep_prob). y is a tensor of shape (N_examples, 10), with values
    equal to the logits of classifying the digit into one of 10 classes (the
    digits 0-9). keep_prob is a scalar placeholder for the probability of
    dropout.
  """
  # Reshape to use within a convolutional neural net.
  # Last dimension is for "features" - there is only one here, since images are
  # grayscale -- it would be 3 for an RGB image, 4 for RGBA, etc.
  with tf.name_scope('reshape'):
    x_image = tf.reshape(x, [-1, 28, 28, 1])

  # First convolutional layer - maps one grayscale image to 32 feature maps.
  with tf.name_scope('conv1'):
    W_conv1 = weight_variable([5, 5, 1, 32])
    b_conv1 = bias_variable([32])
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)

  # Pooling layer - downsamples by 2X.
  with tf.name_scope('pool1'):
    h_pool1 = max_pool_2x2(h_conv1)

  # Second convolutional layer -- maps 32 feature maps to 64.
  with tf.name_scope('conv2'):
    W_conv2 = weight_variable([5, 5, 32, 64])
    b_conv2 = bias_variable([64])
    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)

  # Second pooling layer.
  with tf.name_scope('pool2'):
    h_pool2 = max_pool_2x2(h_conv2)

  # Fully connected layer 1 -- after 2 round of downsampling, our 28x28 image
  # is down to 7x7x64 feature maps -- maps this to 1024 features.
  with tf.name_scope('fc1'):
    W_fc1 = weight_variable([7 * 7 * 64, 1024])
    b_fc1 = bias_variable([1024])

    h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

  # Dropout - controls the complexity of the model, prevents co-adaptation of
  # features.
  with tf.name_scope('dropout'):
    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

  # Map the 1024 features to nb_classes classes, one for each digit
  with tf.name_scope('fc2'):
    W_fc2 = weight_variable([1024, nb_classes])
    b_fc2 = bias_variable([nb_classes])

    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
  return y_conv, keep_prob


def conv2d(x, W):
    """conv2d returns a 2d convolution layer with full stride."""
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
    """max_pool_2x2 downsamples a feature map by 2X."""
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')


def weight_variable(shape):
    """weight_variable generates a weight variable of a given shape."""
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    """bias_variable generates a bias variable of a given shape."""
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def main(_):
    print("Loading data...")
    _x, _y = load.load_data('data/small_emnist_x.csv', 'data/small_emnist_y.csv', False)
    print("Done.")

    nb_features = _x.shape[1]
    nb_classes = np.max(_y) + 1
    nb_examples = _x.shape[0]
    print(nb_classes)

    print("Preprocessing...")
    Y = np.zeros(shape=(nb_examples, nb_classes))
    for i in range(nb_examples):
        Y[i, _y[i]] = 1
    _y = Y

    _x_train, _x_val, _y_train, _y_val = train_test_split(_x, _y, test_size=0.2, random_state=42)
    nb_train_examples = _x_train.shape[0]
    nb_val_examples = _x_val.shape[0]

    x_test = load.load_csv('data/clustered_test_x.csv')
    nb_test_examples = x_test.shape[0]
    print("Done.")

    # Create the model
    x = tf.placeholder(tf.float32, [None, nb_features])

    # Define loss and optimizer
    y_ = tf.placeholder(tf.float32, [None, nb_classes])

    keep_prob = tf.placeholder(tf.float32, [None, nb_classes])

    # Build the graph for the deep net
    y_conv, keep_prob = deepnn(x, nb_classes)

    with tf.name_scope('loss'):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv)
    cross_entropy = tf.reduce_mean(cross_entropy)

    with tf.name_scope('adam_optimizer'):
        train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

    with tf.name_scope('accuracy'):
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
        correct_prediction = tf.cast(correct_prediction, tf.float32)
    accuracy = tf.reduce_mean(correct_prediction)

    graph_location = tempfile.mkdtemp()
    print('Saving graph to: %s' % graph_location)
    train_writer = tf.summary.FileWriter(graph_location)
    train_writer.add_graph(tf.get_default_graph())

    batch_size = 100
    
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.as_default()

        for j in range(100):
            print("Epoch {}".format(j+1))
            train_accuracy = 0.0

            # Fit Phase
            for (beginning, end, i) in batch_split(nb_train_examples, batch_size):
                x_batch = _x_train[beginning:end]
                y_batch = _y_train[beginning:end]
                
                train_step.run(session=sess, feed_dict={x: x_batch, y_: y_batch, keep_prob: 0.5})

                train_accuracy += accuracy.eval(session=sess, feed_dict={x: x_batch, y_: y_batch, keep_prob: 1.0}) * (x_batch.shape[0] / nb_train_examples)
            
            print('training accuracy : {}'.format(train_accuracy))
        
        # Test accuracy on validation set
        acc = 0.0
        for (beginning, end, i) in batch_split(nb_val_examples, batch_size):
            x_batch = _x_val[beginning:end]
            y_batch = _y_val[beginning:end]
            acc += accuracy.eval(session=sess, feed_dict={x: x_batch, y_: y_batch, keep_prob: 1.0}) * (x_batch.shape[0] / nb_val_examples)

        print('test accuracy %g' % acc)

        # Predict Phase
        # prediction = tf.argmax(y_conv,1)
        best = np.zeros((nb_test_examples, nb_classes))

        for (beginning, end, i) in batch_split(nb_test_examples, batch_size):
            x_batch = x_test[beginning:end]
            best[beginning:end] = sess.run(y_conv,feed_dict={x: x_batch, keep_prob: 1.0})

        print(best[0])
        load.write_csv(best, "results/cluster_predicts.csv", "%1.6f")

        # Print results
        for i in range(best.shape[0]):
            best_sort = best[i].argsort()[-3:][::-1]
            best_tuples = zip(best_sort, best[i][best_sort])
            for b in best_tuples:
                print("{}:{}".format(b[0], b[1]))
            print()
            plt.imshow(x_test[i].reshape(28,28))
            plt.show()
        

tf.app.run(main=main)