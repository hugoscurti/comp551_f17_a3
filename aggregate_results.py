import load
import numpy as np
X = load.load_csv("results/cluster_predicts.csv")
y = np.zeros((X.shape[0]//3,))

# Could need it
classes =  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 24, 25, 27, 28, 30, 32, 35, 36, 40, 42, 45, 48, 49, 54, 56, 63, 64, 72, 81]
vals = np.zeros((3,4))
indices = np.zeros((3,4), dtype='i')

operations = [10, 11, 12]

for i in range(X.shape[0]//3):
    
    # Sort predictions
    for k in range(vals.shape[0]):
        # We store the predicted value and its confidence
        indices[k] = X[i*3+k].argsort()[-4:][::-1]
        vals[k] = X[i*3+k][indices[k]]

    # Pick the highest Add or Mult
    depth = 0
    op_ind = -1
    op_val = None
    op_confidence = float("-inf")

    while op_val is None and depth < vals.shape[1]:
        for k, val in enumerate(vals):
            if indices[k][depth] in operations and val[depth] > op_confidence:
                op_val = indices[k][depth]
                op_confidence = val[depth]
                op_ind = k
        
        # Look for threes if we've found no operator
        if op_val is None:
            for k,val in enumerate(vals):
                if indices[k][depth] == 3:
                    op_val = 11
                    op_confidence = val[depth]
                    op_ind = k
                    break

        # Go to next depth if we've found no operator
        if op_val is None:
            depth += 1
    
    if op_val is None:
        # We didn't found any operator, return 0
        print(i)
        print(indices)
        print(vals)
        print("---------------------------")
        y[i] = 0
        continue

    #Get result
    num_vals = []
    for k, val in enumerate(indices):
        if k==op_ind:
            continue
        for d in val:
            if d not in operations:
                num_vals.append(d)
                break
    
    # Compute operation
    if op_val in [10, 12]:
        y[i] = num_vals[0] + num_vals[1]
    else:
        y[i] = num_vals[0] * num_vals[1]
    

# Make sure y is an array of integer
y = y.astype('i')
load.write_y(y, "results/test_y.csv")
