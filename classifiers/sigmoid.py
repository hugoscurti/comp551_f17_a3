import numpy as np

def sigmoid(x):
    #print(x)
    res = 1.0/(1.0+np.exp(-x))

    # If res is a numpy.float (i.e. just an element, no shape), then we return the python float value
    return res if res.shape else res.item()

def sigmoid_backward(dA, cache):
    s = 1/(1+np.exp(-cache))
    return dA * s * (1 - s)
