import numpy as np
from .sigmoid import sigmoid, sigmoid_backward
import h5py

class NeuralNetwork():
    def __init__(self, X, Y, size_hidden_layers = [10]):
        self.nb_classes = Y.shape[1]
        self.size_hidden_layers = size_hidden_layers
        self.nb_features = X.shape[1]
        self.size_layers = list(self.size_hidden_layers)
        self.size_layers.append(self.nb_classes)

        np.random.seed(1)

        self.layers = []
        self.bias = []
        for i in range(len(self.size_layers)):
            nb_units_before = 0
            if(i == 0):
                nb_units_before = self.nb_features
            else:
                nb_units_before = self.size_layers[i - 1]
            self.layers.append(np.random.randn(self.size_layers[i], nb_units_before) * 0.01)
            self.bias.append(np.zeros(shape=(self.size_layers[i], 1)))

        self.X = X.transpose()
        self.Y = Y

    def _linear_forward(self, A, W, b):
        #print(W)
        #print(A)
        return np.dot(W, A) + b, (A, W, b)

    def _linear_activation_forward(self, A_prev, W, b):
        Z, linear_cache = self._linear_forward(A_prev, W, b)
        return sigmoid(Z), (linear_cache, Z)

    def _forward_propagation(self, X):
        A = X
        caches = []
        for i in range(len(self.layers)):
            A, LAF_cache = self._linear_activation_forward(A, self.layers[i], self.bias[i])
            caches.append(LAF_cache)
        return A, caches

    def _cost(self, A):
        m = self.Y.shape[1]
        cost = (-1 / m) * np.sum(np.multiply(self.Y, np.log(A)) + np.multiply(1 - self.Y, np.log(1 - A)))
        return np.squeeze(cost)

    def _linear_backward(self, dZ, cache):
        A_prev, W, b = cache
        m = A_prev.shape[1]

        dA_prev = np.dot(W.transpose(), dZ)
        dW = np.dot(dZ, A_prev.transpose()) / m
        db = np.sum(dZ, axis=1, keepdims=True) / m

        return dA_prev, dW, db

    def _linear_activation_backward(self, dA, cache):
        linear_cache, activation_cache = cache
        dZ = sigmoid_backward(dA, activation_cache)
        return self._linear_backward(dZ, linear_cache)

    def _backward_propagation(self, AL, caches):
        gradients = {}
        m = AL.shape[1]
        #Y_copy = self.Y.reshape(AL.shape)
        Y_copy = self.Y.T
        L = len(self.size_layers)

        dAL = -(np.divide(Y_copy, AL) - np.divide(1 - Y_copy, 1 - AL))
        gradients["dA" + str(L-1)], gradients["dW" + str(L-1)], gradients["db" + str(L-1)] = self._linear_activation_backward(
            dAL, 
            caches[L-1]
        )
        for l in reversed(range(L-1)):
            dA_prev_temp, dW_temp, db_temp = self._linear_activation_backward(gradients["dA" + str(l+1)], caches[l])
            gradients["dA" + str(l)] = dA_prev_temp
            gradients["dW" + str(l)] = dW_temp
            gradients["db" + str(l)] = db_temp

        return gradients

    def _update_weights(self, gradients, learning_rate):
        for l in range(len(self.layers)):
            self.layers[l] -= learning_rate * gradients["dW" + str(l)]
            #print(self.bias[l].shape)
            self.bias[l] -= learning_rate * gradients["db" + str(l)]
            #print(self.bias[l].shape)

    def train(self, epoch, learning_rate, verbose=False):
        prev_cost = float('inf')
        for i in range(epoch):
            AL, caches = self._forward_propagation(self.X)
            cost = self._cost(AL.T)
            if verbose:
                print("epoch {} \tcost : {}".format(i, cost))
            # Look for stagnated learning rate
            if cost - prev_cost > -0.0001:
                learning_rate = learning_rate * 0.5
                if verbose: 
                    print("reduced learning rate ({})".format(learning_rate))

            prev_cost = cost

            gradients = self._backward_propagation(AL, caches)
            self._update_weights(gradients, learning_rate)

    def predict(self, X, mapping, y=None):
        m = X.shape[1]
        n = len(self.layers) // 2 # number of layers in the neural network
        p = np.zeros((1,m))
        
        # Forward propagation
        probas, caches = self._forward_propagation(X)
        #print(probas.shape)
        
        # convert probas to 0/1 predictions
        for i in range(probas.shape[1]):
            c = np.argmax(probas[:, i])
            p[0, i] = mapping[c]
        
        accuracy = None
        if y is not None:
            accuracy = np.sum((p == y)/m)
            print("Accuracy: {}".format(accuracy))
            
        return p, accuracy

    

if __name__ == "__main__":
    X = np.zeros(shape=(100, 2))
    Y = np.zeros(shape=(100, 4))
    Y[1, 2] = 1
    nn = NeuralNetwork(X, Y)
    nn.train(20, 0.5)
