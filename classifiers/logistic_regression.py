
import numpy as np

# User defined libraries
from .sigmoid import sigmoid

class LogisticRegression():
    def __init__(self, max_iter=1000, tol=1e-4, alpha=0.01, learning_rate="static"):
        self.w = None
        self.w0 = None
        self.classes = None

        self.eps = tol
        self.max_iter = max_iter
        self.alpha = alpha

        # Learning rates : 
        #   "static": always use the same alpha
        #   "inv_prop" : inversely proportionnal to the iteration
        #               i.e. alpha / k
        self.learning_rate = learning_rate


    def get_alpha(self, iter):
        """Return the alpha parameter for this iteration,
        with respect to the learning_rate value"""

        if self.learning_rate == "static":
            return self.alpha
        elif self.learning_rate == "inv_prop":
            return self.alpha / iter


    def fit(self, X, y):

        self.classes = np.unique(y) # Get all different classes

        # We need a bias term and a weight vector for each class (since we do a One-Versus-Rest Scheme)
        self.w = np.zeros((X.shape[1], self.classes.shape[0]))
        self.w0 = np.zeros(self.classes.shape[0])

        for i, class_ in enumerate(self.classes):
            # Return 1 for current class and 0 for all other classes
            y_i = np.where(y == class_, 1, 0)

            self.w0[i], self.w[:,i] = self.fit_binomial(X, y_i)


    def fit_binomial(self, X, y):
        # Initialize weights with 1
        w = np.ones(X.shape[1])
        w0 = 0

        k = 1
        tolerance = float('inf')

        while tolerance > self.eps and k < self.max_iter:

            # Get alpha for this round
            alpha_k = self.get_alpha(k)

            # vector of predictions X * w_k + w0
            pred = w0 + X.dot(w)

            # Sigmoid function element-wise
            sigm = sigmoid(pred)
            error = y - sigm

            # Compute gradient
            # We average the gradient over the number of samples
            # We use matrix multiplication to get the gradient for each training sample
            grad =  error.dot(X) / X.shape[0]

            # Update bias term with same gradient equation (although X would be 1, so we sum all error terms)
            w0 += alpha_k * np.sum(error) / X.shape[0]

            # Update weights
            w = w + alpha_k * grad

            #Iterate and update the tolerance factor
            k += 1
            tolerance = np.linalg.norm(grad) # Stricter tolerance term
            #tolerance = np.max(np.abs(grad)) # Same as scikit-learn
        
        # Return both bias term and weight vector
        return w0, w


    def predict(self, X):
        # Predict for each class
        res_clss = np.zeros((X.shape[0], self.classes.shape[0]))
        res = np.zeros(X.shape[0], dtype="i")

        pred = self.w0 + X.dot(self.w)  # Should have the same number of rows than X
        res_clss = sigmoid(pred)   # Sigmoid function for all predictions

        for i, val in enumerate(res_clss):
            res[i] = self.classes[np.argmax(val)]

        return res

