import load

from sklearn.cluster import KMeans
import numpy as np
from scipy.misc import imresize
from scipy.ndimage import center_of_mass
from scipy.ndimage.interpolation import shift
from scipy.ndimage.filters import (median_filter, gaussian_filter)


import os
import argparse
import random
import math

# Help separate values from different matrices when concatenating 
separator = np.ones((64,3)) * 255
sep28 = np.ones((28,2)) * 255

def verbose(i):
    # Show output once in a while
    if i % 100 == 0:
        print("Finished row {}.".format(i))

def filter_noise(X, nclusters=4):
    # We expect X to be the correct shape (i.e. (-1, 64, 64))
    kmeans = KMeans(n_clusters=nclusters)
    X_temp = np.zeros_like(X)

    for i, x in enumerate(X):
        # Fit coordinates into a new matrix of samples/features, where samples are each coordinate
        # and features are x coord, y coord and the value
        tuples = list(zip(*np.nonzero(x)))
        triples = np.array([[tup[0], tup[1], x[tup]] for tup in tuples])

        # Apply kmeans on triples
        res = kmeans.fit_predict(triples)

        # Find cluster which contains the characters 
        # (ie the cluster with the highest greyscale values)
        max = 0
        cluster = -1

        for j in range(nclusters):
            tempmax = np.max(triples[res==j,2])

            if tempmax > max:
                max = tempmax
                cluster = j
        
        #Get filtered x
        filt = triples[res==cluster]
        for val in filt:
            X_temp[i,int(val[0]),int(val[1])] = val[2]

        verbose(i)
       
    return X_temp


def reshape_v2(X, centers):
    crop = 15
    X_new = np.zeros((X.shape[0], 28, 28))
    for i, x in enumerate(X):
        center = center_of_mass(np.ones_like(x))
        temp = shift(x, (center - centers[i]), cval=0.0, prefilter=False, order=0)

        # Crop and filter
        X_new[i] = imresize(temp[crop:-crop, crop:-crop], (28, 28))
        X_new[i] = median_filter(X_new[i], 2)

    return X_new



def reshape_chars(X):
    x_new = np.zeros((X.shape[0], 28, 28))

    for i, x in enumerate(X):
        indices = np.where(x != 0)
        
        # Initialize bounding box
        bounding_box = (np.min(indices[0]), np.max(indices[0]), np.min(indices[1]), np.max(indices[1]))

        exemple_temp = x[bounding_box[0]:bounding_box[1]+1, bounding_box[2]:bounding_box[3]+1]
        if exemple_temp.shape[0] <= 28 and exemple_temp.shape[1] <= 28:
            x_shift = math.ceil((28 - (bounding_box[1] - bounding_box[0] + 1)) / 2)
            y_shift = math.floor((28 - (bounding_box[3] - bounding_box[2] + 1)) / 2)
            new_array = np.zeros((28, 28))
            new_array[x_shift:x_shift + bounding_box[1]+1 - bounding_box[0], y_shift:y_shift + bounding_box[3]+1 - bounding_box[2]] = x[bounding_box[0]:bounding_box[1]+1, bounding_box[2]:bounding_box[3]+1]
        else:
            max_dim = np.max(exemple_temp.shape)
            min_dim = np.min(exemple_temp.shape)

            ratio = 28/max_dim
            x_shift = 0
            y_shift = 0
            if exemple_temp.shape[0] >= exemple_temp.shape[1]:
                y_shift = math.floor((28 - math.ceil(ratio*min_dim)) / 2)
                new_array_temp = imresize(exemple_temp, size=(28, math.ceil(ratio*min_dim)))
            else:
                x_shift = math.ceil((28 - math.ceil(ratio*min_dim)) / 2)
                new_array_temp = imresize(exemple_temp, size=(math.ceil(ratio*min_dim), 28))
            new_array = np.zeros((28, 28))
            new_array[x_shift:x_shift + new_array_temp.shape[0], y_shift:y_shift + new_array_temp.shape[1]] = new_array_temp
        x_new[i] = new_array
    
    return x_new


def cluster_samples(X):
    """Cluster samples from X in groups of 3"""
    kmeans = KMeans(n_clusters=3)
    X_out = np.zeros((X.shape[0]*3, 64, 64))
    centers = np.zeros((X_out.shape[0], 2))

    for i, x in enumerate(X):
        tuples = list(zip(*np.nonzero(x)))

        res = kmeans.fit_predict(tuples)

        # Separate x into 3, based on the kmean clustering result
        for j, tup in enumerate(tuples):
            X_out[i*3 + res[j], tup[0], tup[1]] = x[tup[0], tup[1]]

        for j, center in enumerate(kmeans.cluster_centers_):
            centers[i*3 + j] = center

        verbose(i)

    return X_out, centers

def show_image(X_filt, X_clust, X_test, i):
    load.show_data(np.concatenate((X_test[i], separator, 
        X_filt[i]), axis=1))
    load.show_data(np.concatenate((X_clust[i*3], sep28, 
        X_clust[i*3+1], sep28, X_clust[i*3+2]), axis=1))

def show_all_results(X_filt1, X_clust, X_test, rand):
    # Show side by side result of all steps in graph
    indices = np.random.permutation(X_test.shape[0]) if rand else range(X_test.shape[0])

    for i in indices:
        show_image(X_filt1, X_clust, X_test, i)

def show_result_interactive(X_filt, X_clust, X_test):
    while(True):
        idx = input("Enter an index (between {} and {}) or <enter> to exit: ".format(0, X_test.shape[0]-1))
        if not idx: 
            return

        if idx.isdigit() and int(idx) >= 0 and int(idx) < X_test.shape[0]:
            show_image(X_filt, X_clust, X_test, int(idx))
        else:
            print("Value must be a number between {} and {}".format(0, X_test.shape[0]-1))


def get_args():
    # Define arguments
    parser = argparse.ArgumentParser(
        description="Do a 2 steps preprocessing of images and outputs to files, if they don't exist")
    parser.add_argument('-r', '--random', action='store_true')
    parser.add_argument('-i', '--interactive', action='store_true')

    # Get arguments
    return parser.parse_args()


if __name__ == "__main__":
    filtered_file = "./data/filtered_test_x.csv"
    clustered_file = "./data/clustered_test_x.csv"

    args = get_args()

    print("Load...")
    X_test = load.load_csv("./data/test_x.csv")
    X_test = X_test.reshape(-1, 64, 64)
    print("Done.")

    # Filter
    if os.path.isfile(filtered_file):
        # No need to filter out background, result is already in file
        print("Load filtered samples...")
        X_filt1 = load.load_csv(filtered_file).reshape(-1, 64, 64)
        print("Done.")
    else:
        print("Filter background...")
        X_filt1 = filter_noise(X_test, 4)
        # Write to csv
        
        load.write_csv(X_filt1.reshape(-1, 64*64), filtered_file, "%1.6f")
        print("Done.")

    # Cluster and center
    if os.path.isfile(clustered_file):
        print("Load clustered samples...")
        X_clust = load.load_csv(clustered_file).reshape(-1, 28, 28)
        print("Done.")
    else:
        print("Cluster characters...")
        X_clust, centers = cluster_samples(X_filt1)
        X_clust = reshape_v2(X_clust, centers)
        
        # Write to csv
        load.write_csv(X_clust.reshape(-1, 28*28), clustered_file, "%1.6f")
        print("Done.")

    if args.interactive:
        show_result_interactive(X_filt1, X_clust, X_test)
    else:
        show_all_results(X_filt1, X_clust, X_test, args.random)
    
        