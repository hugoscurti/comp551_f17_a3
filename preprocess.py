import numpy as np
import load

def filter_threshold(X, thresh):
    """Filter input values under the specified threshold"""

    return np.where(X > thresh, X, 0)

def binary_filter_threshold(X,thresh):
    """Filter input values under the specified threshold, sets the other to 1"""
    return np.where(X> thresh, 1, 0)


#define class labels, there are 40 of them
class_labels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 24, 25, 27, 28, 30, 32,
                35, 36, 40, 42, 45, 48, 49, 54, 56, 63, 64, 72, 81]

def preprocess_labels(y):
    """tensorflow requires classes to be between 0 and 40"""
    for i in range(y.shape[0]):
        y[i] = class_labels.index(y[i])
    return y


def postprocess_labels(y):
    """restore original labels"""
    for i in range(y.shape[0]):
        y[i] = class_labels[y[i]]
    return y



if __name__ == "__main__":
    # Small utility program that filters out data from train_x and test_x
    # up to a certain threshold and then shows side by side comparisons of each image in a graph
    X = load.load_csv("./data/test_x.csv")
    X = X.reshape(-1, 64, 64)
    print("Done loading")
    X_pre = filter_threshold(X, 230)

    separator = np.ones((64,3)) * 255
    for i in range(X_pre.shape[0]):
        load.show_data(np.concatenate((X[i], separator, X_pre[i]), axis=1))
