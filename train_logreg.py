import argparse

from sklearn.linear_model import LogisticRegression as SkLogisticRegression
from sklearn.model_selection import KFold
from sklearn import datasets
import sklearn.metrics as metrics
import numpy as np

# User defined libraries
from classifiers.logistic_regression import LogisticRegression
import load
import preprocess as pp


def one_time_train(classifier, threshold):
    """Train the specified classifier on the initial training set 
    and test on the test set. Output file that can be submitted to the kaggle competition. 
    """

    print("Loading data...")
    X, y = load.load_data("./data/train_x.csv", "./data/train_y.csv", False)
    X_test = load.load_csv("./data/test_x.csv")
    print("Done.")

    if threshold > 0:
        print("Preprocessing...")
        X = pp.filter_threshold(X, threshold)
        X_test = pp.filter_threshold(X_test, threshold)
        print("Done.")

    print("Training...")
    classifier.fit(X, y)
    print("Done.")

    print("Classifying...")
    res = classifier.predict(X_test)
    print("Done.")

    # Write result in default file
    load.write_y(res, "./results/test_y.csv")
    

def kfold_train(nb_fold, classifiers, X, y):
    """
    Train all classifiers in the array classifiers using a k-fold validation scheme.
    Return the accuracy for each classifier in a numpy array with the same size as classifiers
    """

    fold = KFold(nb_fold, True)

    # Store accuracies for classifier, accumulate them over the number of fold
    accuracies = np.zeros(len(classifiers))

    print("Training with {}-fold cross-validation...".format(nb_fold))
    for train, test in fold.split(X, y):
        print("New Fold...")

        X_train = X[train]
        X_test = X[test]

        y_train = y[train]
        y_test = y[test]
        
        for i, (name, clssf) in enumerate(classifiers):
            clssf.fit(X_train, y_train)
            y_res = clssf.predict(X_test)
            accuracies[i] += metrics.accuracy_score(y_test, y_res) / 5

    print("Done.\n")

    # Total accuracies for each
    for i, (name, clssf) in enumerate(classifiers):
        print(name)
        print("Accuracy : {}\n".format(accuracies[i]))

    return accuracies


def main(args):
    if args.final:
        classifier = SkLogisticRegression(multi_class="multinomial", solver="saga", verbose=1,
                                          max_iter=args.max_iter)

        one_time_train(classifier, args.threshold)

    else:
        # Test different versions of logistic regression
        #X, y = load.load_data("./data/small_train_x.csv", "./data/small_train_y.csv", False)
        #print("Preprocessing...")
        #X = pp.filter_threshold(X, args.threshold)
        #print("Done.")

        # Test with simple data set
        #ds = datasets.load_breast_cancer()
        ds = datasets.load_digits()
        X = ds.data
        y = ds.target

        # Compare different logistic regression classifiers
        logregs = []

        logregs.append(("Homemade (Default parameters)", LogisticRegression()))

        logregs.append(("Scikit-learn's LogisticRegression", SkLogisticRegression(multi_class="multinomial", solver="saga", verbose=1,
                                                                                  max_iter=args.max_iter)))

        res = kfold_train(5, logregs, X, y)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test different types of logistic regression classifiers.")
    parser.add_argument('-f', '--final', action='store_true')

    parser.add_argument('-i', '--max_iter', type=int, default=100)
    parser.add_argument('-t', '--threshold', type=int, default=230,
                        help="Only used when --final is set")

    args = parser.parse_args()

    main(args)