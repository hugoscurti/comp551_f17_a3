import scipy.io
from scipy.misc import imrotate
import numpy as np
import load as l
import random
import matplotlib.pyplot as plt


def rotate_mult(x, y, maxangle, nbcopies):
    # Instantiate new x and y
    nb_elems = nbcopies + 1
    new_x = np.zeros((x.shape[0]*nb_elems,x.shape[1],x.shape[2]))
    new_y = np.zeros((y.shape[0]*nb_elems,))

    for i, xi in enumerate(x):
        new_x[i*nb_elems] = xi
        new_y[i*nb_elems] = y[i]

        # j starts with 0, we initialize begin_ind to the index for the first copy
        begin_ind = i*nb_elems + 1
        for j in range(nbcopies):
            new_x[begin_ind + j] = imrotate(xi, random.randint(-maxangle, maxangle))
            new_y[begin_ind + j] = y[i]

    return new_x, new_y


def add_noise(x, y, nbcopies):
    """Function that copies the images and adds gaussian noise to the copied versions"""
    nb_elems = nbcopies + 1
    new_x = np.zeros((x.shape[0]*nb_elems,x.shape[1],x.shape[2]))
    new_y = np.zeros((y.shape[0]*nb_elems,))

    for i, xi in enumerate(x):
        new_x[i*nb_elems] = xi
        new_y[i*nb_elems] = y[i]

        # Not sure if these values should be calculated for each image or set arbitrarily
        mean = 128
        var = 70
        sigma = var**0.5

        # j starts with 0, we initialize begin_ind to the index for the first copy
        begin_ind = i*nb_elems + 1
        for j in range(nbcopies):

            noise = np.random.normal(mean, sigma, xi.shape)
            noise = noise.reshape(xi.shape)
            new_x[begin_ind + j] = xi + noise
            new_y[begin_ind + j] = y[i]

    return new_x, new_y


def filter_characters(x, y):
    """Remove all characters except 'm' and 'a'"""
    new_x = x
    new_y = y
    to_delete = []
    for i in range(len(y)):
        # 0 to 9 + A OR a OR M
        if(not((y[i] >= 0 and y[i] <= 10) or y[i] == 36 or y[i] == 22)):
            to_delete.append(i)

    new_x = np.delete(new_x, to_delete, 0)
    new_y = np.delete(new_y, to_delete, 0)

    for i in range(len(new_y)):
        
        if(new_y[i] == 36):
            new_y[i] = 12
        if(new_y[i] == 22):
            new_y[i] = 11
        new_x[i] = new_x[i].reshape(28,28).transpose().reshape(-1)

        #print(new_y[i])
        #plt.imshow(new_x[i].reshape(28,28))
        #plt.show()

    return new_x, new_y

def load_emnist_file(filename):
    print("Loading data...")
    mat = scipy.io.loadmat(filename)
    x = mat['dataset'][0,0][0][0][0][0]
    y = mat['dataset'][0,0][0][0][0][1]
    print("Done.")

    return x,y

def write_data(x, y, file_x, file_y):
    l.write_csv(x, file_x, "%1.6f")   # float with 6 decimal positions
    l.write_csv(y, file_y, "%1.i")    # integer

if __name__ == "__main__":
    print("Loading data...")
    x, y = load_emnist_file("./raw_data/emnist-balanced.mat")
    print("Done.")

    print("Creating smaller data sets...")
    x_new, y_new = filter_characters(x, y)
    x_new, y_new = rotate_mult(x_new.reshape((-1, 28, 28)), y_new, 45, 3)
    #x_new, y_new = add_noise(x_new, y_new, 3)  It turned out adding noise didn't help
    
    # Shuffle data aferwards
    ps = np.random.permutation(x_new.shape[0])
    x_new = x_new[ps]
    y_new = y_new[ps]

    x_new = x_new.reshape(-1, 28*28)
    print(x_new.shape)
    print(y_new.shape)
    write_data(x_new, y_new, "./data/small_emnist_x.csv", "./data/small_emnist_y.csv")
    print("Done.")

    # Show outputted data
    x_show = x_new.reshape(-1, 28, 28)
    for i, xi in enumerate(x_show):
        print(y_new[i])
        l.show_data(xi)
