# Project 3 : Modified digits #

COMP551 : Applied Machine Learning, Fall 2017

Authors :

- Hugo Scurti
- R�mi Martin
- Benjamin Paul-Dubois-Taine
- - -


## Prerequisites ##

This project runs with __Python 3.6__. The project depends on the following libraries : 

- numpy
- scipy
- Pillow
- pandas
- matplotlib
- scikit-learn
- Tensorflow
- Keras
- h5py


## Description ##

This project experiments on a few methods to try to solve the Modified digits classification problem presented [here](http://cs.mcgill.ca/~jpineau/comp551/Project2_instructions.pdf).
Methods include the following : 

- 2 logistic regression classifiers
- A fully implemented Feed-Forward Neural Network
- A Simple ConvNet for single character recognition, using abundant pre-processing and post-processing
- A complexed ConvNet based on [VGG](http://www.robots.ox.ac.uk/~vgg/research/very_deep/)

## Code ##

We explain below how to execute all methods described in our paper.

### Prerequesites ###

Before executing any method below, a few steps should be executed in order to possess all different files necessary.

1. Download the data set files from the kaggle competition (train_x.csv, train_y.csv, test_x.csv) and put them in the project's *data* folder.

2. At the project's root folder, execute `python load.py`, which effectively creates smaller versions of the training sets *train_x* and *train_y* and writes them into *small_train_x.csv* and *small_train_y.csv*.

### Train the Logistic Regression Classifiers ###

To train the logistic regression classifiers, simply execute `python train_logreg.py` at the project's root folder. By default, this will do cross validation on a smaller training set (i.e. the [digits dataset](http://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_digits.html) from scikit-learn) and show results in command-line. 

Additional parameters can be used : 

- `-i [NUM]` or `--max_iter [NUM]`: indicate the maximum number of iterations used by the classifiers;
- `f` or `--final` : instead of doing cross-val. on smaller set, train the multinomial logistic regression classifier on the real train/test set. Results will be written in *test_y.csv* in folder *results*.
- `-t [NUM]` or `--threshold [NUM]`: change the threshold used to filter the images; **This parameter is only used when flag `-f` or `--final` is set**;

### Train the Feed-Forward Neural Network Classifier ###

To train the Feed-Forward NN classifiers, simply execute `python train_FFNN.py` at the project's root folder. By default, this will do cross validation on a smaller training set (i.e. the [digits dataset](http://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_digits.html)) and return the best hyperparameters for this problem set.

The same parameters as above can be used to change the default behaviour. When using flag `-f` (or `--final`), results will be written in file *ffnn_predictions.csv* in folder *results*.


### Train the EMNIST ConvNet ###

In order to train our EMNIST ConvNet, please follow the steps below :

1. Load the emnist data set in matlab version from [here](http://www.itl.nist.gov/iaui/vip/cs_links/EMNIST/matlab.zip) and extract the file *emnist-balanced.mat* into the project's *raw_data* folder;

2. At the project's root folder, execute `python load_emnist.py`, which will create adequate training sets (_small_emnist_x.csv_, _small_emnist_y.csv_).
   We will use these samples to train the ConvNet.

3. At the project's root folder, execute `python clusters.py`, which will split the test set _test_x.csv_ and split the image using 2 steps of k-mean algorithm, as explained in our paper. This will create *filtered_test_x.csv* and *clustered_test_x.csv* (in folder *data*). The latter will be needed for the training step.
   When the program is done, it will show results of the splitting process. It is possible to go into interactive mode and select the indices to show by using `-i` when executing the command. It's possible to pass `-r` to let the program show random images, instead of images in the order in which they appear in the training set.
   If not using the interactive mode, simply interrupt the program when you are done looking at results (otherwise it will go through 10 000 images!). In interactive mode, simply press enter instead of entering an index.

4. **It's time to train!** Execute `python train_EMNIST.py` to start train the EMNIST ConvNet. **This could take a while...** Results will be written in the file *cluster_predicts.csv*.

5. Do the post-processing step : execute `python aggregate_results.py`, which will post-process the results stored in *cluster_predicts.csv* (i.e. aggregate character results together and do the addition or multiplication operation).



### Train the VGG16 ConvNets with preprocessing ###

- Execute `python train_vgg.py`. By default, this will train the VGG16 ConvNet with preprocessing and store models for every epoch in folder *model_checkpoints*.
- To simply predict on a saved model, select the model checkpoint with which to predict, per example, the furthest epoch with the lowest loss value (show in the name of the file), then execute `python train_vgg -p -m "path/to/model.h5py"`.

#### Train the VGG16 ConvNet without preprocessing ####

- Execute `python train_vgg.py -n` to train the model, using the non-preprocessed flag.
- To simply predict on a saved model, select the model checkpoint with which to predict from the previously trained model, then execute `python train_vgg -n -p -m "path/to/model.h5py"`. It's important to keep the non-preprocessed flag at this step as well, since we do not want the test set to be preprocessed either.