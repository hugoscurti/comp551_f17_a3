import load, preprocess
import keras.utils as np_utils
import numpy as np
import vgg
import argparse

# Build argument parse from command line
parser = argparse.ArgumentParser(description="Test Convolutional Neural Net using keras.")
parser.add_argument('-p', '--predict', action='store_true',
                    help="Set flag to only predict, not fit the model")

parser.add_argument('-m', '--model_path', type=str, default='',
                    help="If --predict, predict the model from the saved model. If not --predict, fit the model from the saved one")

parser.add_argument('-n', '--no_preprocess', action='store_true',
                    help='Doesn\'t preprocess the training and test samples.')

args = parser.parse_args()

# Data sets
x_train = "./data/train_x.csv"
y_train = "./data/train_y.csv"
x_test = "./data/test_x.csv"
NUM_CLASSES = 40

if not args.predict:
    x_train = load.load_csv(x_train)
    y_train = load.load_csv(y_train)

    # preprocess class labels to only have labels between 0 and 40
    y_train = preprocess.preprocess_labels(y_train)
    y_train = np_utils.to_categorical(y_train,NUM_CLASSES)
    x_train = x_train.reshape(x_train.shape[0],64,64,1)

x_test = load.load_csv(x_test)
x_test = x_test.reshape(x_test.shape[0],64,64,1)

if not args.no_preprocess:
    # preprocess images
    threshold = 230
    if not args.predict:
        x_train = preprocess.binary_filter_threshold(x_train, threshold)
    x_test = preprocess.binary_filter_threshold(x_test, threshold)

#get predictions from model
if not args.predict:
    if args.model_path:
        predictions = vgg.vgg_from_file(x_test, file=args.model_path,
                              batch_size=100, train=True, x_train=x_train, y_train=y_train, epochs=100)
    else:
        predictions, history = vgg.vgg(x_train,y_train,x_test=x_test, batch_size=100, epochs=100)
else:
    predictions = vgg.vgg_from_file(x_test, file=args.model_path, train=False)

#remove one hot encoding
predictions = np.argmax(predictions, axis=1)
predictions = preprocess.postprocess_labels(predictions)
#write on csv file
load.write_y(predictions,'results/vgg_predictions.csv')