import sys
import argparse
from sklearn import datasets
from sklearn.model_selection import KFold
import classifiers.NeuralNetwork as NN
import preprocess as pp
from random import seed
import numpy as np
import load


def to_one_hot(y):
    classes = np.unique(y).tolist()
    nb_classes = len(classes)
    Y = []
    for c in y:
        vec = [0] * nb_classes
        vec[classes.index(c)] = 1
        Y.append(vec)
    Y = np.array(Y)

    return Y, classes


def main(args):
    seed(1)

    if args.final:
        # Now we test on the real dataset
        #X, y = load.load_data("./data/small_train_x.csv", "./data/small_train_y.csv", False)
        X, y = load.load_data("./data/train_x.csv", "./data/train_y.csv", False)
        X_test = load.load_csv("./data/test_x.csv")


        if args.threshold > 0:
            print("Preprocessing...")
            X = pp.filter_threshold(X, args.threshold)
            X_test = pp.filter_threshold(X_test, args.threshold)
            print("Done.")

        # Conver Y to one hot
        Y, classes = to_one_hot(y)

        # THose parameters are chosen after doing k-fold (i.e. without flag --final)
        hiddenlayers = [60]
        learningrate = 0.8

        onekfold = KFold(X.shape[0]//10)

        # We just do one fold with 10% of held out data to look at the validation accuracy
        for train, test in onekfold.split(X):
            X_train = X[train]
            Y_train = Y[train]
            X_val = X[test]
            Y_val = y[test]

            classifier = NN.NeuralNetwork(X_train, Y_train, hiddenlayers)
            classifier.train(args.max_iter, learning_rate=learningrate, verbose=True)
            pr, acc = classifier.predict(X_val.transpose(), classes, Y_val)
            print("Validation accuracy : {}".format(acc))
            break

        pr, _ = classifier.predict(X_test.transpose(), classes)
        load.write_y(pr.astype('i').flatten(), "results/ffnn_predictions.csv")

    else:
        ds = datasets.load_digits()
        X = ds.data
        y = ds.target

        Y, classes = to_one_hot(y)

        # Hyperparameter to tune
        hiddenlayers = [
            [30],       [50],
            [60],       [60, 30],
            [50, 20],   [50, 50]
        ]

        # Our neural net takes high learning rate values
        learningrate = [
            0.01,   0.05,
            0.2,    0.4,
            0.6,    0.8
        ]

        fivefold = KFold(5, True)

        res = np.zeros((len(hiddenlayers), len(learningrate)))
        fold_i = 1
        for train, test in fivefold.split(X):
            X_train = X[train]
            Y_train = Y[train]  # One hot for trainig
            X_test = X[test]
            Y_test = y[test] # Multiclass for testing

            print("Fold {}...".format(fold_i))
            fold_i += 1
            for i in range(len(hiddenlayers)):
                for j in range(len(learningrate)):
                    nn = NN.NeuralNetwork(X_train, Y_train, size_hidden_layers=hiddenlayers[i])
                    nn.train(args.max_iter, learningrate[j], False)
                    _, acc = nn.predict(X_test.transpose(), classes, Y_test)
                    res[i, j] += acc / 5 # Divide by number of folds

        # Pick the best hyperparameters
        best_h = 0
        best_l = 0
        for i in range(len(hiddenlayers)):
            for j in range(len(learningrate)):
                if res[i, j] > res[best_h, best_l]:
                    best_h = i
                    best_l = j
        print("Best Accuracy : {} \tLearning rate: {}, \tHidden Layers: {}".format(
            res[best_h, best_l], learningrate[best_l], hiddenlayers[best_h]))



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test different types of logistic regression classifiers.")
    parser.add_argument('-f', '--final', action='store_true')

    parser.add_argument('-i', '--max_iter', type=int, default=1000)
    parser.add_argument('-t', '--threshold', type=int, default=230,
                        help="Only used when --final is set")

    args = parser.parse_args()

    main(args)
